package com.grouptasks.arraysorter;

public class ArraySorter {


    public void sortByBits(int[] array) {
        int leftIndex = 0;
        int rightIndex = array.length - 1;
        sort(array, leftIndex, rightIndex);
    }

    private int sumOfBits(int number) {
        int sum = 0;
        int bitLimit = 32;
        for(int i = 0; i < bitLimit; i++) {
            sum += number & 1;
            number >>= 1;
        }
       
        return sum;
    }
    
    private void sort(int arr[], int left, int right) {
        if (left < right) {
            // Find the middle point
            int middle = (left + right) / 2;
            
            // Sort first and second halves
            sort(arr, left, middle);
            sort(arr, middle + 1, right);
            
            // Merge the sorted halves
            merge(arr, left, middle, right);
        }
    }
    
    private void merge(int array[], int left, int middle, int right) {
        
        // Find sizes of two subarrays to be merged
        int sizeOne = middle - left + 1;
        int sizeTwo = right - middle;

        /* Create temp arrays */
        int leftTempArray[] = new int[sizeOne];
        int rightTempArray[] = new int[sizeTwo];

        /* Copy data to temp arrays */
        for (int i = 0; i < sizeOne; ++i) {
            leftTempArray[i] = array[left + i];
        }
        
        for (int j = 0; j < sizeTwo; ++j) {
            rightTempArray[j] = array[middle + 1 + j];
        }

        /* Merge the temp arrays */

        // Initial indexes of first and second subarrays
        int indexFirst = 0, indexSecond = 0;

        // Initial index of merged subarry array
        int k = left;
        while (indexFirst < sizeOne && indexSecond < sizeTwo) {
            if (sumOfBits(leftTempArray[indexFirst]) <= sumOfBits(rightTempArray[indexSecond])) {
                array[k] = leftTempArray[indexFirst];
                indexFirst++;
            } else {
                array[k] = rightTempArray[indexSecond];
                indexSecond++;
            }
            k++;
        }

        /* Copy remaining elements of L[] if any */
        while (indexFirst < sizeOne) {
            array[k] = leftTempArray[indexFirst];
            indexFirst++;
            k++;
        }

        /* Copy remaining elements of R[] if any */
        while (indexSecond < sizeTwo) {
            array[k] = rightTempArray[indexSecond];
            indexSecond++;
            k++;
        }
    }

}
