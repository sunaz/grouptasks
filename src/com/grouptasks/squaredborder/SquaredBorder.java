package com.grouptasks.squaredborder;

public class SquaredBorder {

    private int[][] array;

    public SquaredBorder(int[][] array) {
        this.array = array;
    }

    public boolean check() {
        for (int i = 0; i < array.length - 1; i++) {

            int currentNumber = 0;
            for (int j = 0; j < array[i].length - 1; j++) {

                currentNumber = array[i][j];
                if (checkIfFibonacciNumber(currentNumber)) {
                    int currentRow = i;
                    int currentColumn = j;
                    if (isFibonacciSquare(currentRow, currentColumn)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean isFibonacciSquare(int currentRow, int currentColumn) {
    	int firstCell = array[currentRow][currentColumn];
    	int secondCell = array[currentRow][currentColumn+1];
    	int thirdCell = array[currentRow+1][currentColumn+1];
    	int fourthCell = array[currentRow+1][currentColumn];
        if(checkIfFibonacciNumber(secondCell) 
       		&& checkIfFibonacciNumber(thirdCell)
        	&&	checkIfFibonacciNumber(fourthCell)){
        	return ( (firstCell + secondCell == thirdCell) && (secondCell + thirdCell == fourthCell))
        			||( (secondCell + thirdCell == fourthCell) && (thirdCell + fourthCell == firstCell))
        			||( (thirdCell + fourthCell == firstCell) && (fourthCell + firstCell == secondCell))
        			||( (fourthCell + firstCell == secondCell) && (firstCell + secondCell == thirdCell));
        	
        }
        return false;
    }

    private boolean checkIfFibonacciNumber(int number) {
        // n is Fibonacci if one of 5*n*n + 4 or 5*n*n - 4 or both
        // is a perfect square
        int sideOne = 5 * number * number + 4;
        int sideTwo = 5 * number * number - 4;
        return isPerfectSquare(sideOne) || isPerfectSquare(sideTwo);
    }

    private boolean isPerfectSquare(int square) {
        int side = (int) Math.sqrt(square);
        return (side * side == square);
    }
}
