package com.grouptasks.matrixofneighbours;

import java.util.ArrayList;
import java.util.Collections;

public class MatrixOfNeighbours {

    private int[][] matrix;

    public MatrixOfNeighbours(int[][] matrix) {
        this.matrix = matrix;
    }

    public boolean check() {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (isFibonacciNeighbours(i, j)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isFibonacciNeighbours(int i, int j) {
        ArrayList<Integer> neighbours = getNeighbours(i, j);
        Collections.sort(neighbours);

        if (isFibonacciSequence(neighbours)) {
            return true;
        }
        return false;
    }

    private boolean isFibonacciSequence(ArrayList<Integer> neighbours) {
        int currentNumber = 0;
        int[] previousNumbers = new int[2];
        
        if(!checkInitialNeighbours(previousNumbers, neighbours)) {
            return false;
        }
        
        for(int i = 2; i < neighbours.size(); i++) {
            currentNumber = neighbours.get(i);
            if(!isFibonacciSum(previousNumbers, currentNumber)) {
                return false;
            }
            previousNumbers[0] = previousNumbers[1];
            previousNumbers[1] = currentNumber;
        }
        return true;
    }

    private boolean isFibonacciNumber(int number) {
        int sideOne = 5 * number * number + 4;
        int sideTwo = 5 * number * number - 4;
        return isPerfectSquare(sideOne) || isPerfectSquare(sideTwo);
    }

    private boolean isPerfectSquare(int square) {
        int side = (int) Math.sqrt(square);
        return (side * side == square);
    }

    private boolean isFibonacciSum(int[] previousNumbers, int currentNumber) {
        int expectedSum = previousNumbers[0] + previousNumbers[1];
        return currentNumber == expectedSum;
    }

    public ArrayList<Integer> getNeighbours(int i, int j) {
    	ArrayList<Integer> list = new ArrayList<Integer>();
        for(int firstIndex = i - 1; firstIndex < i + 2; firstIndex++) {
        	for(int secondIndex = j - 1; secondIndex < j + 2; secondIndex++) {
            	if(firstIndex >= 0 && firstIndex < matrix.length) {
            		if(secondIndex >=0 && secondIndex < matrix[i].length) {
            			if(firstIndex != i || secondIndex != j) {
            				list.add(matrix[firstIndex][secondIndex]);
            			}
            		}
            	}
            }
        }
        return list;
    }

    private boolean checkInitialNeighbours(int[] previousNumbers, ArrayList<Integer> neighbours) {
        if(neighbours.size() == 1) {
            return isFibonacciNumber(neighbours.get(0));
        } else if(neighbours.size() >= 2) {
            previousNumbers[0] = neighbours.get(0);
            previousNumbers[1] = neighbours.get(1);
            
            return (isFibonacciNumber(previousNumbers[0]) 
                    && isFibonacciNumber(previousNumbers[1])
                    && isFibonacciNumber(previousNumbers[0] + previousNumbers[1]));
        } 
        return false;
    }
}
