package com.grouptasks.arraysorter;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ArraySorterTest {

    private ArraySorter arraySorter;

    @Before
    public void setUp() {
        arraySorter = new ArraySorter();
    }

    @Test
    public void givenEmptyArrayWhenSortByBitsIsInvokedThenArrayIsStillEmpty() {
        int[] givenArray = {};
        int[] expectedArray = {};

        arraySorter.sortByBits(givenArray);

        assertArrayEquals(expectedArray, givenArray);
    }

    @Test
    public void givenOneElementArrayWhenSortByBitsIsInvokedThenArrayIsStillSame() {
        int[] givenArray = { 1 };
        int[] expectedArray = { 1 };

        arraySorter.sortByBits(givenArray);

        assertArrayEquals(expectedArray, givenArray);
    }

    @Test
    public void givenPositiveElementArrayWhenSortByBitsIsInvokedThenArrayIsSort() {
        int[] givenArray = { 0, 1, 12, 37, 48, 19, 10, 98, 83, 61 };
        int[] expectedArray = { 0, 1, 12, 48, 10, 37, 19, 98, 83, 61 };

        arraySorter.sortByBits(givenArray);

        assertArrayEquals(expectedArray, givenArray);
    }

    @Test
    public void givenNegativeElementArrayWhenSortByBitsIsInvokedThenArrayIsSort() {
        int[] givenArray = { -9, -2, -19, -13, -21, -102, -31, -32, -16 };
        int[] expectedArray = { -32, -102, -31, -16, -19, -13, -21, -9, -2 };

        arraySorter.sortByBits(givenArray);

        assertArrayEquals(expectedArray, givenArray);
    }

    @Test
    public void givenIntegerArrayWhenSortByBitsIsInvokedThenArrayIsSort() {
        int[] givenArray = { 12, 0, 1, -32, -9, 10, 61, -102, -16, 19 };
        int[] expectedArray = { 0, 1, 12, 10, 19, 61, -32, -102, -16, -9 };

        arraySorter.sortByBits(givenArray);

        assertArrayEquals(expectedArray, givenArray);
    }

    @Test
    public void givenSortedByBitsArrayWhenSortByBitsIsInvokedThenArrayIsSame() {
        int[] givenArray = { 0, 1, 12, 10, 19, 61, -32, -102, -16, -9 };
        int[] expectedArray = { 0, 1, 12, 10, 19, 61, -32, -102, -16, -9 };

        arraySorter.sortByBits(givenArray);

        assertArrayEquals(expectedArray, givenArray);
    }

    @Test
    public void givenReversedArrayWhenSortByBitsIsInvokedThenArrayIsReversed() {
        int[] givenArray = { -9, -16, -32, 61, 19, 12, 1, 0 };
        int[] expectedArray = { 0, 1, 12, 19, 61, -32, -16, -9 };

        arraySorter.sortByBits(givenArray);

        assertArrayEquals(expectedArray, givenArray);
    }
}
