	package com.grouptasks.squaredborder;

import static org.junit.Assert.*;

import org.junit.Test;

public class SquaredBorderTest {

	@Test
    public void givenEmptyArrayWhenCheckIsInvokedThenReturnFalse() {
        int[][] givenArray = {};
        SquaredBorder squaredBorder = new SquaredBorder(givenArray);
        
        assertFalse(squaredBorder.check());
    }

    @Test
    public void givenArrayWithoutFibonacciNumbersWhenCheckIsInvokedThenReturnFalse() {
        int[][] givenArray = {{4, 6, 7, 9},
                              {9, 7, 6, 4},
                              {22, 23, 15, 18},
                              {4, 50, 99, 100}};
        SquaredBorder squaredBorder = new SquaredBorder(givenArray);
        
        assertFalse(squaredBorder.check());
    }
    
    @Test
    public void givenArrayWithFibonacciNumbersPlacedInLeftUpperCorners() {
        int[][] givenArray = {{5, 8, 7, 9},
                              {21, 13, 6, 4},
                              {22, 23, 15, 18},
                              {4, 50, 99, 100}};
        SquaredBorder squaredBorder = new SquaredBorder(givenArray);
        
        assertTrue(squaredBorder.check());
    }
    
    @Test
    public void givenBigArrayWithFibonacciNumbersWithSmallSquareExpectTrue() {
        int[][] givenArray = { {1,2,3,4,5,5}, 
        						{4,8,6,4,5,3}, 
        						{7,5,2,3,0,4} , 
								{4,45,8,5,8,2}, 
        						{123,52,45,63,41,3},
        						{456,132,213,123,123,2} };
        SquaredBorder squaredBorder = new SquaredBorder(givenArray);
        
        assertTrue(squaredBorder.check());
    }
    
    @Test
    public void givenBigArrayWithFibonacciNumbersWithBigSquare() {
        int[][] givenArray = { {1,2,3,4,5,5}, 
        						{4,8,6,4,5,3}, 
        						{7,5,2,3,5,4} , 
								{4,45,55,89,8,2}, 
        						{123,52,34,21,13,3},
        						{456,132,213,123,123,2} };
        SquaredBorder squaredBorder = new SquaredBorder(givenArray);
        
        assertTrue(squaredBorder.check());
    }
    
    @Test
    public void givenBigArrayWithFibonacciNumbersWithBigRotatedSquare() {
        int[][] givenArray = { {1,2,3,4,5,5}, 
        						{4,8,6,4,5,3}, 
        						{7,5,13,21,34,4} , 
								{4,45,8,89,55,2}, 
        						{123,52,5,3,2,3},
        						{456,132,213,123,123,2} };
        SquaredBorder squaredBorder = new SquaredBorder(givenArray);
        
        assertTrue(squaredBorder.check());
    }
    
    @Test
    public void givenArrayWithFibonacciNumbersPlacedInRightUpperCorners() {
        int[][] givenArray = {{5, 4, 610, 987},
                              {6, 13, 2584, 1597},
                              {22, 23, 4, 18},
                              {4, 50, 99, 100}};
        SquaredBorder squaredBorder = new SquaredBorder(givenArray);
        
        assertTrue(squaredBorder.check());
    }
    
    @Test
    public void givenArrayWithFibonacciNumbersPlacedInLeftBottomUpperCorners() {
        int[][] givenArray = {{5, 4, 2, 987},
                              {6, 13, 2584, 4},
                              {0, 1, 4, 18},
                              {2, 1, 99, 100}};
        SquaredBorder squaredBorder = new SquaredBorder(givenArray);
        
        assertTrue(squaredBorder.check());
    }
    
    @Test
    public void givenArrayWithFibonacciNumbersPlacedInRightBottomUpperCorners() {
        int[][] givenArray = {{5, 4, 2, 987},
                              {6, 13, 2584, 4},
                              {5, 1, 13, 21},
                              {2, 6, 55, 34}};
        SquaredBorder squaredBorder = new SquaredBorder(givenArray);
        
        assertTrue(squaredBorder.check());
    }
    
    @Test
    public void givenArrayWithFibonacciNumbersPlacedWithinBorder() {
        int[][] givenArray = {{5, 4, 2, 987},
                              {6, 2, 3 , 4},
                              {5, 8, 5, 41},
                              {2, 6, 55, 34}};
        SquaredBorder squaredBorder = new SquaredBorder(givenArray);
        
        assertTrue(squaredBorder.check());
    }
    
    @Test
    public void givenArrayWithFibonacciNumbersWithoutProperSquare() {
        int[][] givenArray = {{1, 2, 3, 5},
                              {3, 8, 1 ,8},
                              {144, 89, 34, 55},
                              {610, 8, 233, 34}};
        SquaredBorder squaredBorder = new SquaredBorder(givenArray);
        
        assertFalse(squaredBorder.check());
    }
}
