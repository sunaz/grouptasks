package com.grouptasks.matrixofneighbours;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

public class MatrixOfNeighboursTest {

	@Test
	public void givenNeighbourFibonacciSequenceWhenCheckThenReturnTrue() {
		int[][] givenArray = { { 8, 5, 3, 6 }, 
								{ 13, 3, 2, 4 }, 
								{ 0, 1, 1, 18 }, 
								{ 2, 1, 99, 100 } };
		MatrixOfNeighbours matrixOfNeighbours = new MatrixOfNeighbours(givenArray);
		
		assertTrue( matrixOfNeighbours.check());
	}

	@Test
	public void givenNeighbourFibonacciSequenceInCornerWhenCheckThenReturnTrue() {
		int[][] givenArray = { { 1, 3, 3, 6 }, 
								{ 1, 2, 2, 4 }, 
								{ 0, 1, 1, 18 }, 
								{ 2, 1, 99, 100 } };
		MatrixOfNeighbours matrixOfNeighbours = new MatrixOfNeighbours(givenArray);
		
		assertTrue( matrixOfNeighbours.check());
	}
	
	@Test
	public void givenNeighbourFibonacciSequenceOnBorderWhenCheckThenReturnTrue() {
		int[][] givenArray = { { 1, 1, 3, 8 }, 
								{ 1, 5, 3, 2 }, 
								{ 0, 1, 1, 18 }, 
								{ 2, 1, 99, 100 } };
		MatrixOfNeighbours matrixOfNeighbours = new MatrixOfNeighbours(givenArray);
		
		assertTrue( matrixOfNeighbours.check());
	}
	
	@Test
	public void givenArrayWithoutNeighbourWhenCheckThenReturnFalse() {
		int[][] givenArray = {{ 2 }};
		MatrixOfNeighbours matrixOfNeighbours = new MatrixOfNeighbours(givenArray);

		assertFalse(matrixOfNeighbours.check() );
		
	}
	
	@Test
	public void givenArrayWithOneNeighbourWhenCheckThenReturnTrue() {
		int[][] givenArray = {{ 2, 1 }};
		MatrixOfNeighbours matrixOfNeighbours = new MatrixOfNeighbours(givenArray);
		
		assertTrue(matrixOfNeighbours.check() );
		
	}
	
	@Test
	public void givenArrayWithOneNeighbourWhenCheckThenReturnFalse() {
		int[][] givenArray = {{ 7, 9 }};
		MatrixOfNeighbours matrixOfNeighbours = new MatrixOfNeighbours(givenArray);
		
		assertFalse(matrixOfNeighbours.check() );
		
	}
	
	@Test
	public void givenArrayWithTwoNeighbourWhenCheckThenReturnTrue() {
		int[][] givenArray = {{ 2, 9, 3}};
		MatrixOfNeighbours matrixOfNeighbours = new MatrixOfNeighbours(givenArray);

		assertTrue(matrixOfNeighbours.check() );
		
	}
	
}
