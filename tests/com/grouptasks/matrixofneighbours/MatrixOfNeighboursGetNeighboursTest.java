package com.grouptasks.matrixofneighbours;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

public class MatrixOfNeighboursGetNeighboursTest {

	@Test
	public void testGetNeighboursWithCornerIndex() {
		int[][] givenArray = {{2, 1, 2, 987},
                			{2, 3, 2584, 4},
                			{0, 1, 4, 18},
                			{2, 1, 99, 100}};
		MatrixOfNeighbours matrixOfNeighbours = new MatrixOfNeighbours(givenArray);
		ArrayList test = new ArrayList(Arrays.asList(1, 2, 3));

		assertEquals(test ,matrixOfNeighbours.getNeighbours(0, 0) );
		
	}
	
	@Test
	public void testGetNeighboursWithBorderIndex() {
		int[][] givenArray = {{2, 1, 2, 6},
                			{2, 3, 5, 4},
                			{0, 1, 4, 18},
                			{2, 1, 99, 100}};
		MatrixOfNeighbours matrixOfNeighbours = new MatrixOfNeighbours(givenArray);
		ArrayList test = new ArrayList(Arrays.asList(1, 6, 3, 5, 4));

		assertEquals(test ,matrixOfNeighbours.getNeighbours(0, 2) );
		
	}

	@Test
	public void testGetNeighboursWithNormalIndex() {
		int[][] givenArray = {{2, 1, 2, 6},
                			{2, 3, 5, 4},
                			{0, 1, 4, 18},
                			{2, 1, 99, 100}};
		MatrixOfNeighbours matrixOfNeighbours = new MatrixOfNeighbours(givenArray);
		ArrayList test = new ArrayList(Arrays.asList(3, 5, 4, 1, 18, 1, 99, 100));

		assertEquals(test ,matrixOfNeighbours.getNeighbours(2, 2) );
		
	}
	
	@Test
	public void testGetNeighboursWithOneElementArray() {
		int[][] givenArray = {{ 2 }};
		MatrixOfNeighbours matrixOfNeighbours = new MatrixOfNeighbours(givenArray);
		ArrayList test = new ArrayList(Arrays.asList());

		assertEquals(test ,matrixOfNeighbours.getNeighbours(0, 0) );
		
	}
	
	@Test
	public void testGetNeighboursWithTwoElementArray() {
		int[][] givenArray = {{ 2, 1 }};
		MatrixOfNeighbours matrixOfNeighbours = new MatrixOfNeighbours(givenArray);
		ArrayList test = new ArrayList(Arrays.asList(1));

		assertEquals(test ,matrixOfNeighbours.getNeighbours(0, 0) );
		
	}
	
	@Test
	public void testGetNeighboursWithThreeElementArray() {
		int[][] givenArray = {{ 2, 1, 3}};
		MatrixOfNeighbours matrixOfNeighbours = new MatrixOfNeighbours(givenArray);
		ArrayList test = new ArrayList(Arrays.asList(2, 3));

		assertEquals(test ,matrixOfNeighbours.getNeighbours(0, 1) );
		
	}
}
